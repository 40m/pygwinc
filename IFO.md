# Canonical IFOs

CI-generated plots and data for all IFOs included in pygwinc.

![IFO compare](https://gwinc.docs.ligo.org/pygwinc/all_compare.png)

## aLIGO

* [ifo.yaml](gwinc/ifo/aLIGO/ifo.yaml)
* [aLIGO.h5](https://gwinc.docs.ligo.org/pygwinc/aLIGO.h5)

![aLIGO](https://gwinc.docs.ligo.org/pygwinc/aLIGO.png)


## A+

* [ifo.yaml](gwinc/ifo/Aplus/ifo.yaml)
* [Aplus.h5](https://gwinc.docs.ligo.org/pygwinc/Aplus.h5)

![Aplus](https://gwinc.docs.ligo.org/pygwinc/Aplus.png)


## Voyager

* [ifo.yaml](gwinc/ifo/Voyager/ifo.yaml)
* [Voyager.h5](https://gwinc.docs.ligo.org/pygwinc/Voyager.h5)

![Voyager](https://gwinc.docs.ligo.org/pygwinc/Voyager.png)


## Cosmic Explorer 1

* [ifo.yaml](gwinc/ifo/CE1/ifo.yaml)
* [CE1.h5](https://gwinc.docs.ligo.org/pygwinc/CE1.h5)

![CE1](https://gwinc.docs.ligo.org/pygwinc/CE1.png)


## Cosmic Explorer 2

* [ifo.yaml](gwinc/ifo/CE2/ifo.yaml)
* [CE2.h5](https://gwinc.docs.ligo.org/pygwinc/CE2.h5)

![CE2](https://gwinc.docs.ligo.org/pygwinc/CE2.png)
