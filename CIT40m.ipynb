{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Noise budget for the 40m BHD experiment\n",
    "The 40m noise budget is now fully integrated into pygwinc. As before, pygwinc can be used for estimating a variety of fundamental noise sources (e.g., quantum noise, thermal noises). However custom, IFO-specific noise terms can now be defined within the pygwinc framework. User-defined noises can supply an arbitrary model and/or load real measurement data. This notebook illustrates the basic usage.\n",
    "\n",
    "## Noise Definitions\n",
    "40m noise term definitions are contained in ```./CIT40m/__init__.py```.\n",
    "\n",
    "To add a new noise:\n",
    "1. Create a new noise class in ```__init__.py``` following the template of the existing noises.\n",
    "2. Add the new class name to the list inside the noise budget class definition (```CIT40m```).\n",
    "\n",
    "## Measurement Data\n",
    "The subdirectory ```./CIT40m/data``` contains noise and calibration measurement data.\n",
    "\n",
    "Any file added to this directory is automatically tracked via Git LFS (i.e., the file is replaced with a text pointer to a remote storage location). Data files can be safely added to this directory in the usual way (```$ git add ...```).\n",
    "\n",
    "#### Created J. Richardson Apr 07 2020"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import h5py\n",
    "import scipy\n",
    "import scipy.signal\n",
    "import scipy.io\n",
    "import scipy.constants as scc\n",
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "import gwinc\n",
    "\n",
    "# Load MPL style file\n",
    "mpl.style.use('CIT40m/style40.mpl')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "R = 4500.   #coil driver series resistance (Ohm)  \n",
    "P = 20.     #input power (W)        \n",
    "phi = -0.01 #SRC detuning (deg)\n",
    "zeta = 0.   #homodyne phase (deg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluate 40m noise budget"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the frequency bins [Hz]\n",
    "freq = np.logspace(np.log10(10), np.log10(1e4), 1000)\n",
    "\n",
    "# Load the 40m budget class\n",
    "Budget = gwinc.load_budget('CIT40m')\n",
    "\n",
    "# Optionally override class parameters\n",
    "Budget.ifo.Optics.SRM.Tunephase = np.pi - np.deg2rad(phi)\n",
    "Budget.ifo.Optics.Quadrature.dc = np.deg2rad(zeta)\n",
    "Budget.ifo.Laser.Power = P\n",
    "\n",
    "# Evaluate the budget\n",
    "# Traces in units [m^2/Hz]\n",
    "budget = Budget(freq)\n",
    "traces = budget.run()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluate aLIGO noise budget (for comparison)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "Budget_ = gwinc.load_budget('aLIGO')\n",
    "budget_ = Budget_(freq)\n",
    "traces_ = budget_.run()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Generate figure: 40m noise budget"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1,1, figsize=(14,9))\n",
    "\n",
    "# Title\n",
    "SRCstr = '$\\\\phi_{{\\\\mathrm{{SRC}}}} = {:.2f}^{{\\\\circ}}$, '.format(round(phi, 2))\n",
    "homStr = '$\\\\zeta = {:.1f}^{{\\\\circ}}$, '.format(round(zeta, 1))\n",
    "powStr = '$P_{{\\\\mathrm{{in}}}}$ = {:.0f} W'.format(round(P, 0))\n",
    "ax.set_title('40m DARM sensitivity for ' + SRCstr + homStr + powStr)\n",
    "\n",
    "# Noise curves\n",
    "for key in traces:\n",
    "    trace = traces[key][0]**.5\n",
    "    style = traces[key][1]\n",
    "    ax.plot(freq, trace, **style)\n",
    "\n",
    "# Plot formatting\n",
    "ax.set_xlim(freq[0], freq[-1])\n",
    "ax.set_ylim(5e-22, 2e-15)\n",
    "ax.set_xlabel(u\"Frequency [Hz]\")\n",
    "ax.set_ylabel(u\"Displacement [m/\\u221AHz]\")\n",
    "ax.set_xscale('log')\n",
    "ax.set_yscale('log')\n",
    "ax.legend(loc='upper right', ncol=1);\n",
    "\n",
    "# Uncomment to save figure\n",
    "# fig.savefig('40m_nb.pdf', bbox_inches='tight')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Generate figure: 40m comparison to aLIGO"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1,1, figsize=(14,9))\n",
    "\n",
    "# Title\n",
    "SRCstr = '$\\\\phi_{{\\\\mathrm{{SRC}}}} = {:.2f}^{{\\\\circ}}$, '.format(round(phi, 2))\n",
    "homStr = '$\\\\zeta = {:.1f}^{{\\\\circ}}$, '.format(round(zeta, 1))\n",
    "powStr = '$P_{{\\\\mathrm{{in}}}}$ = {:.0f} W'.format(round(P, 0))\n",
    "ax.set_title('40m DARM sensitivity for ' + SRCstr + homStr + powStr)\n",
    "\n",
    "ax.plot(freq, traces['Total'][0]**.5, linewidth=6, label=u\"Total 40m\", color='k')\n",
    "ax.plot(freq, traces_['Total'][0]**.5 * budget_.ifo.Infrastructure.Length, color='y', \n",
    "        linewidth=6, label='aLIGO design')\n",
    "\n",
    "# Plot formatting\n",
    "ax.set_xlim(freq[0], freq[-1])\n",
    "ax.set_ylim(5e-22, 2e-15)\n",
    "ax.set_xlabel(u\"Frequency [Hz]\")\n",
    "ax.set_ylabel(u\"Displacement [m/\\u221AHz]\")\n",
    "ax.set_xscale('log')\n",
    "ax.set_yscale('log')\n",
    "ax.legend(loc='upper right', ncol=1);\n",
    "\n",
    "# Uncomment to save figure\n",
    "# fig.savefig('40m_vs_aLIGO.pdf', bbox_inches='tight')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
