# 40m Noise Budget

The 40m noise budget is now fully integrated into pygwinc.
As before, pygwinc can be used for estimating a variety of fundamental noise 
sources (e.g., quantum noise, thermal noises). However custom, IFO-specific 
noise terms can now be defined within the pygwinc framework. User-defined noises 
can supply an arbitrary model and/or load real measurement data.

## Usage

For example usage, see `40mNB.ipynb` in the root level.

## Noise Definitions

The 40m noise term definitions are contained in `./__init__.py`.

To add a new noise:

1. Create a new noise class in `__init__.py` following the template of the existing noises.
2. Add the new class name to the list inside the noise budget class definition (`CIT40m`).

## Measurement Data

The `./data` subdirectory contains noise and calibration measurement data.

Any file added to this directory is automatically tracked via Git LFS (i.e., 
the file is replaced with a text pointer to a remote storage location). Data 
files can be safely added to this directory in the usual way (`$ git add ...`).
