## 40M NOISE BUDGET
## All noises are in units of DISPLACEMENT POWER [m^2/Hz]
##
from gwinc import noise
from gwinc import nb

import scipy
import scipy.constants as scc
import numpy as np

from . import noises40 as n40


class QuantumVacuum(nb.Noise):
    """Quantum Vacuum
    """
    style = dict(
        label='Quantum Vacuum',
        #color='#8c408c',
        linewidth='4',
        linestyle='-',
    )

    def calc(self):
        n40.precomp_mirror(self.freq, self.ifo)
        n40.precomp_gwinc(self.freq, self.ifo)
        qn = noise.quantum.shotrad(self.freq, self.ifo)
        #qn *= self.ifo.Infrastructure.Length**2
        return qn


class CoatingBrownian(nb.Noise):
    """Coating brownian noise for the arm cavities
    """
    style = dict(
        label='Coating Brownian',
        #color='#b36b6e',
        linewidth='4',
        linestyle='-',
    )

    def calc(self):
        wavelength = self.ifo.Laser.Wavelength
        materials = self.ifo.Materials
        w0, wBeam_ITM, wBeam_ETM = n40.arm_cavity(self.ifo)
        dOpt_ITM = n40.coating_thickness(self.ifo, 'ITM')
        dOpt_ETM = n40.coating_thickness(self.ifo, 'ETM')
        nITM = noise.coatingthermal.coating_brownian(
            self.freq, materials, wavelength, wBeam_ITM, dOpt_ITM)
        nETM = noise.coatingthermal.coating_brownian(
            self.freq, materials, wavelength, wBeam_ETM, dOpt_ETM)
        return (nITM + nETM) * 2

class CoatingThermoOptic(nb.Noise):
    """Coating Thermo-Optic

    """
    style = dict(
        label='Coating Thermo-Optic',
        #color='#02ccfe',
        linewidth='4',
        linestyle='-',
    )

    def calc(self):
        wavelength = self.ifo.Laser.Wavelength
        materials = self.ifo.Materials
        w0, wBeam_ITM, wBeam_ETM = n40.arm_cavity(self.ifo)
        dOpt_ITM = n40.coating_thickness(self.ifo, 'ITM')
        dOpt_ETM = n40.coating_thickness(self.ifo, 'ETM')
        nITM, junk1, junk2, junk3 = noise.coatingthermal.coating_thermooptic(
            self.freq, materials, wavelength, wBeam_ITM, dOpt_ITM[:])
        nETM, junk1, junk2, junk3 = noise.coatingthermal.coating_thermooptic(
            self.freq, materials, wavelength, wBeam_ETM, dOpt_ETM[:])
        return (nITM + nETM) * 2

class SuspensionThermal(nb.Noise):
    """Suspension thermal noise
    """
    style = dict(
        label='Suspension Thermal',
        #color='#996bed',
        linewidth='4',
        linestyle='-',
    )

    def calc(self):
        n = n40.suspSOS(self.freq, self.ifo)
        return  4 * n**2


class CoilDriver(nb.Noise):
    """Coil driver noise
       Model from elog 13146
    """
    style = dict(
        label='Coil driver',
        #color='#800000',
        linewidth='4',
        linestyle='-',
    )

    def calc(self):
        n = np.sqrt(4*scc.k*self.ifo.Infrastructure.Temp/self.ifo.Electronics.Rseries)
        n *= 0.032
        n /= self.ifo.Optics.Mass
        n /= (2*np.pi*self.freq)**2
        return  2 * n**2


class Seismic(nb.Noise):
    """Seismic noise
       Measurement from elog 13146
    """
    style = dict(
        label='Seismic',
        #color='#805e00',
        linewidth='4',
        linestyle='-',
    )

    def calc(self):
        data = scipy.io.loadmat('CIT40m/data/seis40.mat')
        ff = data['darmseis_f'][0,:]
        asd = data['darmseis_x'][0,:] #This is displacement noise ASD, not strain.
        seis = 10**self.interpolate(ff, np.log10(asd), kind='linear')
        seis = seis**2 * 4 #quadrature sum of 4 suspended masses.
        return seis


class DAC(nb.Noise):
    """DAC voltage noise
       Model from elogs 13003, 13146
    """
    style = dict(
        label='DAC',
        #color='#00ff00',
        linewidth='4',
        linestyle='-',
    )

    def calc(self):
        dewhit = n40.dewhiten(self.freq)
        vNoise = 700 * np.sqrt(1 + 100 / self.freq) * 1e-9
        xNoise = n40.volt2darm(self.freq, vNoise, self.ifo.Electronics.Rseries)
        dac = xNoise**2 * np.abs(dewhit)**2
        return dac


class CIT40m(nb.Budget):
    """40m noise budget
    """

    name = 'Caltech 40m'

    # Style of the total noise trace
    style = dict(
        label='Total',
        color='#000000',
        linewidth='8',
        linestyle='-',
    )

    # Only noise classes in this list are included in
    # the calculated budget
    noises = [
        QuantumVacuum,
        SuspensionThermal,
        CoatingBrownian,
        CoatingThermoOptic,
        CoilDriver,
        Seismic,
        DAC,
    ]

    # Uncomment for traces in units of strain [1/Hz]
    # Leave commented for units of displacement [m^2/Hz]
    #calibrations = [
    #    Strain,
    #]
