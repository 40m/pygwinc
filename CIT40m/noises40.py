## ANCILLARY METHODS FOR THE 40M NOISE BUDGET
##
import numpy as np
import scipy
import scipy.constants as scc

import gwinc


def suspSOS(f, ifo):
    '''
    Transcription of MATLAB gwinc calculation of suspension thermal
    noise for an SOS optic (susp1.m)
    gautam, 2019 sometime

    Parameters:
    -----------
    f: array_like
        Frequency vector on which to evaluate noise (Hz)
    ifo: gwinc struct
        GWINC struct object containing all the material properties

    Returns:
    ----------
    TNx: array_like
        Displacement noise ASD for a SINGLE optic
    '''
    w = 2 * np.pi * f                      # Angular frequency vector
    # Some SOS params
    M = ifo.Suspension.Stage.Mass               # SOS mass
    R = ifo.Materials.MassRadius                # SOS radius
    H = ifo.Materials.MassThickness             # SOS thickness
    J = M * (3 * R**2 + H**2) / 12              # Moment of inertia for PIT/YAW
    Jx = M * R**2 /2                            # Moment of inertia for rotation
    rhow = ifo.Suspension.C88Steel.Rho          # Steel density
    r = ifo.Suspension.Stage.WireRadius         # Wire radius
    A = np.pi * r**2                            # Wire cross-sectional area
    rho = rhow * A                              # Linear mass density
    h = ifo.Suspension.Stage.h                  # Vertical dist. from standoff to CoM
    l = ifo.Suspension.Stage.Length             # Pendulum length
    dcm = ifo.Suspension.Stage.dStandoff        # Diameter of wire standoff
    b = np.sqrt(R**2 - h**2)                    # half-distance between bottom attachment points
    E = ifo.Suspension.C88Steel.Y               # Young's modulus
    I = 0.25 * np.pi * r**4                     # Area moment of inertia
    phiw = ifo.Suspension.C88Steel.Phi          # Mechanical loss angle
    EI = E * I * (1 + 1j*phiw)                  # Complex parameter used in formulae
    a = ifo.Suspension.Stage.a                  # Half-distance between top attachment points
    L = np.sqrt((b-a)**2 + (l-h)**2)            # Suspension wire length
    alpha = np.arctan((b-a) / (l-h))            # Wire angle with vertical
    T = M * scipy.constants.g * (l-h) / 2 / L   # Tension in each wire
    alphate = ifo.Suspension.C88Steel.Alpha     # CTE for steel @ 290 K
    c = ifo.Suspension.C88Steel.C               # Specific heat
    kappa = ifo.Suspension.C88Steel.K           # Thermal conductivity
    d = ifo.Suspension.Stage.deCenter           # Optional mis-centering of beam
    kb = scipy.constants.k                      # Boltzmann's constant
    Tb = ifo.Suspension.Temp                    # Temperature

    sigma = rhow * c * (2*r)**2 / 13.55 / kappa
    # TE damping factor
    phite = E * alphate**2 * Tb / (rhow*c) * (sigma*w) / (1 + (sigma*w)**2)

    # Complex Young's modulus
    Ecomplex = E * (1 + 1j*phiw + 1j*phite)
    EI = Ecomplex * I # Factor used in formulae
    sqt = np.sqrt(T**2 + 4 * EI * rho * w**2)
    k = np.sqrt(-T + sqt) / np.sqrt(2*EI)
    ke = np.sqrt(T + sqt) / np.sqrt(2*EI)

    Delta = 1 / ke
    D = (1 - (k*Delta)**2) * np.sin(k*L) - 2*(k*Delta)*np.cos(k*L)
    Kxx = 2 * T * k * (1 + (k*Delta)**2) * (np.cos(k*L) + k*Delta*np.sin(k*L)) / D
    Kxt = Kxx * (h+Delta)
    Ktx = Kxt + 2 * T * (k*Delta)**2
    Ktt = 2 * T * (h+Delta) * (1 + (k*Delta)**2) * ((1 + k**2 * h * Delta) * np.sin(k*L) + k * (h - Delta) * np.cos(k*L) ) / D

    # Matrix determinant
    Det = (Kxx - M * w**2) * (Ktt - J * w**2) - Kxt*Ktx
    Num = (Ktt - J * w**2) - d * (Kxt + Ktx) + d**2 * (Kxx - M * w**2)

    # Displacement thermal noise
    Yxx = 1j * w * Num / Det
    TNx = np.sqrt(4 * kb * Tb * np.real(Yxx) / w**2 )
    return(TNx)


def precomp_mirror(f, ifo):
    ifo.Materials.MirrorVolume = \
        np.pi * ifo.Materials.MassRadius**2 \
        * ifo.Materials.MassThickness
    ifo.Materials.MirrorMass = \
        ifo.Materials.MirrorVolume \
        * ifo.Materials.Substrate.MassDensity


def precomp_gwinc(f, ifo):
    ifo.gwinc = gwinc.Struct()
    pbs, parm, finesse, prfactor, Tpr = ifo_power(ifo)
    ifo.gwinc.pbs = pbs
    ifo.gwinc.parm = parm
    ifo.gwinc.finesse = finesse
    ifo.gwinc.prfactor = prfactor


def ifo_power(ifo, PRfixed=True):
    """Compute power on beamsplitter, finesse, and power recycling factor.

    """
    c = scc.c
    pin = ifo.Laser.Power
    t1 = np.sqrt(ifo.Optics.ITM.Transmittance)
    r1 = np.sqrt(1 - ifo.Optics.ITM.Transmittance)
    r2 = np.sqrt(1 - ifo.Optics.ETM.Transmittance)
    t5 = np.sqrt(ifo.Optics.PRM.Transmittance)
    r5 = np.sqrt(1 - ifo.Optics.PRM.Transmittance)
    loss = ifo.Optics.Loss  # single TM loss
    bsloss = ifo.Optics.BSLoss
    acoat = ifo.Optics.ITM.CoatingAbsorption
    pcrit = ifo.Optics.pcrit

    # Finesse, effective number of bounces in cavity, power recycling factor
    finesse = 2*np.pi / (t1**2 + 2*loss)  # arm cavity finesse
    neff    = 2 * finesse / np.pi

    # Arm cavity reflectivity with finite loss
    garm = t1 / (1 - r1*r2*np.sqrt(1-2*loss))  # amplitude gain wrt input field
    rarm = r1 - t1 * r2 * np.sqrt(1-2*loss) * garm

    if PRfixed:
        Tpr = ifo.Optics.PRM.Transmittance  # use given value
    else:
        Tpr = 1-(rarm*np.sqrt(1-bsloss))**2  # optimal recycling mirror transmission
        t5 = np.sqrt(Tpr)
        r5 = np.sqrt(1 - Tpr)
    prfactor = t5**2 / (1 + r5 * rarm * np.sqrt(1-bsloss))**2

    pbs  = pin * prfactor  # BS power from input power
    parm = pbs * garm**2 / 2  # arm power from BS power

    thickness = ifo.Optics.ITM.get('Thickness', ifo.Materials.MassThickness)
    asub = 1.3 * 2 * thickness * ifo.Optics.SubstrateAbsorption
    pbsl = 2 *pcrit / (asub+acoat*neff)  # bs power limited by thermal lensing

    if pbs > pbsl:
        logging.warning('P_BS exceeds BS Thermal limit!')

    return pbs, parm, finesse, prfactor, Tpr


def coating_thickness(ifo, optic):
    optic = ifo.Optics.get(optic)
    if 'CoatLayerOpticalThickness' in optic:
        return optic['CoatLayerOpticalThickness']
    T = optic.Transmittance
    dL = optic.CoatingThicknessLown
    dCap = optic.CoatingThicknessCap
    return gwinc.noise.coatingthermal.getCoatDopt(ifo.Materials, T, dL, dCap=dCap)


def arm_cavity(ifo):
    L = ifo.Infrastructure.Length

    g1 = 1 - L / ifo.Optics.Curvature.ITM
    g2 = 1 - L / ifo.Optics.Curvature.ETM
    gcav = np.sqrt(g1 * g2 * (1 - g1 * g2))
    gden = g1 - 2 * g1 * g2 + g2

    if (g1 * g2 * (1 - g1 * g2)) <= 0:
        raise Exception('Unstable arm cavity g-factors.  Change ifo.Optics.Curvature')
    elif gcav < 1e-3:
        logging.warning('Nearly unstable arm cavity g-factors.  Reconsider ifo.Optics.Curvature')

    ws = np.sqrt(L * ifo.Laser.Wavelength / np.pi)
    w1 = ws * np.sqrt(np.abs(g2) / gcav)
    w2 = ws * np.sqrt(np.abs(g1) / gcav)

    # waist size
    w0 = ws * np.sqrt(gcav / np.abs(gden))
    zr = np.pi * w0**2 / ifo.Laser.Wavelength
    z1 = L * g2 * (1 - g1) / gden
    z2 = L * g1 * (1 - g2) / gden

    # waist, input, output
    return w0, w1, w2


def volt2darm(ff, vNoise, R):
    """Propagates voltage noise at suspension coils to
       DARM displacement

       Assumes equal RMS contribution from
           4 coils/optic x 2 optics (ETMs)
    """
    # Convert the voltage noise to current noise
    # R is coil driver series resistance
    iNoise = vNoise / R
    # Convert the current noise to force noise
    # 0.016 N/A per coil, total of 4 coils
    fNoise = iNoise * 0.016 * np.sqrt(4)
    # Convert force noise to displacement noise of ONE optic
    # Pendulum TF above the resonant freq in the Laplace domain
    xNoise = fNoise / (2 * np.pi*ff)**2 / 0.25 #mirror mass
    # Project to DARM displacement
    # 2 suspended ETMs
    xNoise = xNoise * np.sqrt(2)
    return xNoise


def dewhiten(ff):
    """Nominal de-whitening filter
       (i.e. 2 Biquads + output filter)
    """
    zs = 2*np.pi*np.array([67.1549+1j*74.6484, 67.1549-1j*74.6484,
                  74.3449+1j*64.965, 74.3449-1j*64.965,
                  127.34, 538.73], dtype='complex')
    ps = 2*np.pi*np.array([10.9451+1j*11.0006, 10.9451-1j*11.0006,
                  10.2078+1j*10.3572,10.2078-1j*10.3572,
                  13.892, 3254.0], dtype='complex')
    k = 1.0271*np.prod(ps)/np.prod(zs)
    tf = scipy.signal.freqs_zpk(zs, ps, k, 2*np.pi*ff)[1]
    return tf
